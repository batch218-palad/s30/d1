// Creat documents to use for the discussion
db.fruits.insertMany([
		{
		    name : "Apple",
		    color : "Red",
		    stock : 20,
		    price: 40,
		    supplier_id : 1,
		    onSale : true,
		    origin: [ "Philippines", "US" ]
		 },
		 {
		     name : "Banana",
		     color : "Yellow",
		     stock : 15,
		     price: 20,
		     supplier_id : 2,
		     onSale : true,
		     origin: [ "Philippines", "Ecuador" ]
		 },
		 {
		     name : "Kiwi",
		     color : "Green",
		     stock : 25,
		     price: 50,
		     supplier_id : 1,
		     onSale : true,
		     origin: [ "US", "China" ]
		 },
		 {
		 	 name : "Mango",
		 	 color : "Yellow",
		 	 stock : 10,
		 	 price: 120,
		 	 supplier_id : 2,
		 	 onSale : false,
		 	 origin: [ "Philippines", "India" ]
		 }

	]);

// What is aggregation?
	// Aggregate:
	// to collect or gather into a mass or whole
	// Reference: https://www.merriam-webster.com/dictionary/aggregate

/*
	- Used to generate, manipulate, and perform operations to create filtered results that helps us to analyze data.
*/



// Using the aggregate method:
	/*
		- The "$match" is used to pass the document that meet the specified condition(s) to the next stage/aggregation process.
- Syntax:
			{$match: {field: value}}
		- The "$group" is used to group elements together and field-value pairs the data from the grouped elements.
		-Syntax:
			{$group: {_id: "value", fieldResult: "valueResult"}}

		- Using both "$match" and"$group" along with aggregation will find for products that are on sale and will group the total amount of stock for all suppliers found.
- Syntax
			db.collectionName.aggregate([{$match:{fieldA: valueA}}, {$group: {_id: "value", fieldResult: "valueResult"}}])

        // -The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
		
	*/



db.fruits.aggregate([
		{$match: {onSale: true} },
		{$group: { _id: "$supplier_id", total: {$sum: "$stock"}} }
	]);

/*

db.fruits.aggregate([
		{$match: {onSale: true} },
		{$group: {_id: "$color"} }
	]);

*/

// Field projection with aggregation
	/*
		-The "$project" can be used when aggregating data to include/exclude fields from the returned result.

		-Syntax:
			{$project: {field: 1/0}}

	*/


db.fruits.aggregate([
		{$match: {onSale:true}},
		{$group: {_id: "$supplier_id", total: {$sum:"$stock"}}},
		{$project: {_id:0}}
	]);


// Sorting Aggregated results
	/*
		- The "$sort" can be used to change the order of thew aggregated result
		-Syntax:
			{$sort: {field:1/-1}}
			1 -> lowest to highest
			-1 -> highest to lowest

	*/



db.fruits.aggregate([
		{$match: {onSale:true}},
		{$group: {_id: "$supplier_id", total: {$sum:"$stock"}}},
		{$sort: {total:1}}
	]);  // sorting base on total , lowest to highest


db.fruits.aggregate([
		{$match: {onSale:true}},
		{$group: {_id: "$supplier_id", total: {$sum:"$stock"}}},
		{$sort: {total:-1}}
	]);  // sorting base on total , highest to lowest


// Aggregating results based on array fields

	/*
		- The "$unwind" deconstructs an array field from a collection/field with an array value to output a result.
		- Syntax:
		 - {$unwind: field}
	*/





db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id:"$origin", fruits: {$sum:1}}}
							// counts the total existence of an origin
							// per existence has a value of 1
			//_id - required field name
			// next to our _id field is a user/programmer created field
	]);


// [SECTION] Other Aggregate Stages

// $count
// count all yellow fruits

db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{count: "Yellow Fruits"}
	]);
// count starts with 0
// increments by 1 if founs existing in document

// ---------
// $avg
// get the average value of stock

db.fruits.aggregate([
		{$match: {color:"Yellow"}},
		{$group: {_id: "$color", yellow_fruits_stock: {$avg: "$stock"}}}
	]);


// $min & $max

// $min - minimum / lowest value in a specified field
// $max - maximum / highest value in a specified field


db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$group: {_id: "$color", yellow_fruits_stock: {$min: "$stock"}}}
	]);


db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$group: {_id: "$color", yellow_fruits_stock: {$max: "$stock"}}}
	]);


















